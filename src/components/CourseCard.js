import {Row, Col, Button, Card} from 'react-bootstrap';
import {useState,useEffect,useContext} from 'react';

import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){

		const {_id,name,description,price} = courseProp;

		/*
			The "course" in the CourseCard component is called a "prop" which is a shorthand for property

			The curly braces are used for props to
		*/

	// user the state hook for this component to be able to stores its state
	// states are used to keep track of information related to individual components
		// const [getter, setter] = useState(initialGetterValue);

	const [enrollees, setEnrollees] = useState(0);
	const [seats, setSeats] = useState(30);
	
	// add new state that will declare whether the button is disabled or not
	const [isDisabled, setIsDisabled] = useState(false);

	// Initial value of enrollees state
		// console.log(enrollees);

	// if you want to change/reassign  the value of the state
		// setEnrollees(1);
		// console.log(enrollees);

	const {user} = useContext(UserContext);

	function enroll(){
		if(seats !== 0){
			setEnrollees(enrollees+1)
			setSeats(seats-1)
			if(seats ===1){
				alert("Congrats!")
			}
		}
		else {
			alert("No more seats!")
		}
	}

/*	
	Define a 'useEffect' hook to have the "CourseCard" component to perform a certain task

	This will run automatically.
	SyntaxL:
		useEffect(sideEffect/function, [dependencies]);

	sideEffect/function - it will run on the first load and will reload depending on the dependency array
*/


	useEffect(()=>{
		if(seats === 0){
			setIsDisabled(true);
		}
	}, [seats]);


	return(
		<Row className="mt-5">
			<Col>
				<Card>
			      <Card.Body>
			        <Card.Title>{name}</Card.Title>
			        <Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>{description}</Card.Text>
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>PhP {price}</Card.Text>
			        <Card.Subtitle>Enrollees:</Card.Subtitle>
			        <Card.Text>{enrollees}</Card.Text>
			        <Card.Subtitle>Available Seats:</Card.Subtitle>
			        <Card.Text>{seats}</Card.Text>
			        {
			        	user ?
       					<Button as = {Link} to ={`/course/${_id}`} disabled={isDisabled}>Enroll</Button>
       					:
       					<Button as = {Link} to ='/login'>View Details</Button>
			        }
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}