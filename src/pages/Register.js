import {Button, Form} from 'react-bootstrap';
import {Navigate,useNavigate} from 'react-router-dom';

import Swal from 'sweetalert2';

// import the hooks that are needed in our page
import {useEffect,useState,Fragment,useContext} from 'react';
import UserContext from '../UserContext.js';

export default function Register(){
	// create 3 new states where we will store the vale from input of the email, password and confirmPassword

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const navigate = useNavigate();

	// const [user, setUser] = useState(localStorage.getItem('email'));
	const {user,setUser}=useContext(UserContext);

	// create another state for the button
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if(email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [email,password,confirmPassword])

	function register(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method: 'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {

			console.log(data)

			if(data === false){
				Swal.fire({
					title: 'Registration failed!',
					icon: 'error',
					text: 'Please try again!'
				})
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
					method: 'POST',
					headers: {'Content-Type':'application/json'},
					body: JSON.stringify({
						email: email,
						password: password
					})

				})
				.then(result => result.json())
				.then(data => {

					localStorage.setItem('token', data.auth);
					retrieveUserDetails(localStorage.getItem('token'))

					Swal.fire({
						title: 'Registration successful!',
						icon: 'success',
						text: 'Welcome to Zuitt!'
					})

					navigate("/");
				})
			}

		})

/*		localStorage.setItem("email", email)
		setUser(localStorage.getItem("email"))

		alert("Congratulations, you are now registered on our website!");

		setEmail('');
		setPassword('');
		setConfirmPassword('');

		navigate("/")*/
	}

	const retrieveUserDetails = (token) => {
		// the token sent as part of the request's header information
		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	return (
		user ?
		<Navigate to = "*"/>
		:
		<Fragment>
			<h1 className="text-center mt-5">Register</h1>
		    <Form className="mt-5" onSubmit={event => register(event)}>

		      <Form.Group className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control
		        	type="email" 
		        	placeholder="Enter email"
		        	value={email}
		        	onChange={event => setEmail(event.target.value)}
		        	required />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicPassword">
		        <Form.Label>Password</Form.Label>
		        <Form.Control
		        	type="password"
		        	placeholder="Password"
		        	value={password}
		        	onChange={event => setPassword(event.target.value)}
		        	required />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
		        <Form.Label>Confirm Password</Form.Label>
		        <Form.Control
		        	type="password"
		        	placeholder="Confirm your password"
		        	value={confirmPassword}
		        	onChange={event => setConfirmPassword(event.target.value)}
		        	required />
		      </Form.Group>

		      {/*
				in this code block we do conditional rendering depending on the state of our isActive
		      */}
		      {
		      	isActive ?
		      	<Button variant="primary" type="submit">
		      	  Submit
		      	</Button>
		      	:
		      	<Button variant="danger" type="submit" disabled>
		      	  Submit
		      	</Button>
		      }

		    </Form>
		</Fragment>
	)
}