import {Button, Form} from 'react-bootstrap';
import {Fragment} from 'react';

import {useState,useEffect,useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';

import Swal from 'sweetalert2';

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const navigate = useNavigate();

	// const [user, setUser] = useState(localStorage.getItem('email'));

	// allows us to consume the UserContext object and it's properties for user validation;
	const {user,setUser}=useContext(UserContext);


	// create another state for the button
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [email,password])

	function login(event){
		event.preventDefault();

		/*
		process a fetch request to corresponding backend API
			Syntax: fetch('url',{options});
		*/
		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: 'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(result => result.json())
		.then(data => {
			console.log(data)

			if(data === false){
				Swal.fire({
					title: 'Authentication failed!',
					icon: 'error',
					text: 'Please try again!'
				})
			}else{
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(localStorage.getItem('token'))

				Swal.fire({
					title: 'Authentication successful!',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})

				navigate("/");
			}

		})


		/*
		if you want to add the email of the authenticated user in the local storage
			Syntax:
				localStorage.setItem('propertyNAme', value)
		*/

		/*localStorage.setItem("email", email)

		setUser(localStorage.getItem("email"))

		alert("You are now signed in!");

		setEmail('');
		setPassword('');*/

		// navigate("/")
	}

	const retrieveUserDetails = (token) => {
		// the token sent as part of the request's header information
		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	return (
		user ?
		<Navigate to = "/*"/>
		:
		<Fragment>
			<h1 className="text-center mt-5">Login</h1>
		    <Form className="mt-5" onSubmit={event => login(event)}>

		      <Form.Group className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control
		        	type="email" 
		        	placeholder="Enter email"
		        	value={email}
		        	onChange={event => setEmail(event.target.value)}
		        	required />
		        <Form.Text className="text-muted">
		          Login your account.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="formBasicPassword">
		        <Form.Label>Password</Form.Label>
		        <Form.Control
		        	type="password"
		        	placeholder="Password"
		        	value={password}
		        	onChange={event => setPassword(event.target.value)}
		        	required />
		      </Form.Group>

		      {
		      	isActive ?
		      	<Button variant="primary" type="submit">
		      	  Login
		      	</Button>
		      	:
		      	<Button variant="danger" type="submit" disabled>
		      	  Login
		      	</Button>
		      }

		    </Form>
		</Fragment>
	)
}