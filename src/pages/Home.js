import {Fragment} from 'react';

import Banner from '../components/Banner.js';
import Highlight from '../components/Highlight.js';


export default function Home(){
	return (
		<Fragment>
			<Banner/>
			<Highlight/>
		</Fragment>
		)
}