
import {Link} from 'react-router-dom';
import {Container} from 'react-bootstrap';

export default function NotFound() {
	return (
		<Container className = "text-center mt-5">
			<h1>PAGE NOT FOUND!</h1>
			<p>Go back to the homepage by clicking the link below:</p>
       		<Link to="/">back to Zuitt</Link>
		</Container>
	)
}