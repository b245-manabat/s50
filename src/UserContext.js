import React from 'react';

/*
	create a context object
		- a context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the app.
		- the context object is a different approach passing information between components and allows easier access by avoiding the use of prop drilling.
*/

const UserContext = React.createContext();

// The "Provider" property of createContext allows other components to consume or use the context object and supply the necessary information;
export const UserProvider = UserContext.Provider;

export default UserContext;