/*
	to create a react app,
		npx create-react-app project-name

	to install bootstrap
		npm install react-bootstrap bootstrap

	to run our react application
		npm start

	files to be removed:

	from folder src:
		App.test.js
		index.css
		reportWebVitals.js
		logo.svg
	
	we need to delete all the importations of the said files
		App.js
		index.js

	ctrl+shift+p or command palette
	- install control package
	- package control install package
	- babel

	then select JavaScript(Babel) below


	the 'import' statement allows us to use the code/export modules from other files similar to how we use the "require" function in Node.js

	React JS it applies the concepts of rendering and mounting in order to display and create components
		- rendering refers to the process of calling/invoking a component returning set of instructions creating DOM
		- mounting is when React JS "renders of deisplays" the component the initial DOM based on the instructions


	using the event.target.value will capture the value inputted by the user on our input area

	in the dependencies in useEffect
		- single dependency [dependecy]
			- on the initial load of the component the side effect/function will be triggered and whenever changes occur on our dependency
		- empty dependency []
			- the side effect will only run during the initial load
		- multiple dependencies [dependency1,dependency2, . . .]
			- the side effect will run during the initial load and whenever the state of the dependencies change


[SECTION] react-router-dom
	for us to be able to use the module/library across all of our pages, we have to contain them with BrowserRouter/Router

	Routes - we contain all of the routes in our react-app
	Route - specific route wherein we will declare the end point or the url and also the component or page to be mounted

	we use Link if we want to go from one page to another page
	we use NavLink if we want to change our page using our NavBar


	The "localStorage.setItem" allows us to manipulate the browser's localStorage property to store information indefinitely

[SECTION] Environtment Variables
	environment variables are important in hiding the sensitive pieces of information like the backend API which can be exploited if added directly into our code

*/